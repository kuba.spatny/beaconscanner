package cz.kubaspatny.beaconscanner.notification

import android.app.Notification
import android.content.Context
import androidx.core.app.NotificationCompat
import cz.kubaspatny.beaconscanner.R

class ServiceNotification(private val context: Context) {
    private var notificationBuilder: NotificationCompat.Builder = NotificationCompat.Builder(
        context,
        NotificationChannelType.CHANNEL_SERVICE.getRegisteredChannelId(context)
    )

    val id = 1

    /**
     * Vrati objekt Notifikace, aniz by ji ukazal. O zobrazeni notifikace
     * se stara system pri volani Service#startForeground().
     */
    val notification: Notification
        get() {
            notificationBuilder.setContentTitle(context.getText(R.string.service_running))
                .setSmallIcon(R.drawable.ic_bluetooth_search)
                .setAutoCancel(false)
                .setCategory(NotificationCompat.CATEGORY_SERVICE)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setOngoing(true)

            val notification = notificationBuilder.build()
            notification.flags = notification.flags or Notification.FLAG_NO_CLEAR

            return notification
        }
}