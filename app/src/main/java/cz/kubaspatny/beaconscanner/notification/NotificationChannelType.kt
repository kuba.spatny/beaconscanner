package cz.kubaspatny.beaconscanner.notification

import android.annotation.TargetApi
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.core.app.NotificationManagerCompat
import cz.kubaspatny.beaconscanner.R

/**
 * Created by kubaspatny on 21/02/2018.
 */
enum class NotificationChannelType(
  private val channelId: String,
  private val channelName: Int,
  private val channelImportance: Int
) {
  CHANNEL_SERVICE(
    "1_ServiceChannel",
    R.string.service_notifications,
    NotificationManagerCompat.IMPORTANCE_LOW
  ){
    @TargetApi(Build.VERSION_CODES.O)
    override fun setNotificationChannelProperties(channel: NotificationChannel) {
      super.setNotificationChannelProperties(channel)
      channel.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
    }
  };

  @TargetApi(Build.VERSION_CODES.O)
  protected fun createNotificationChannel(
    channelId: String,
    channelName: String,
    channelImportance: Int
  ): NotificationChannel {
    val channel = NotificationChannel(channelId, channelName, channelImportance)
    setNotificationChannelProperties(channel)

    return channel
  }

  @TargetApi(Build.VERSION_CODES.O)
  protected open fun setNotificationChannelProperties(channel: NotificationChannel) {
    channel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
    channel.setShowBadge(false)
  }

  fun getRegisteredChannelId(context: Context): String {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
      val channel =
        createNotificationChannel(channelId, context.getString(channelName), channelImportance)

      val notificationManager =
        context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
      notificationManager.createNotificationChannel(channel)
    }

    return channelId
  }

  companion object {
    fun registerNotificationChannels(context: Context) {
      NotificationChannelType.values().forEach {
        it.getRegisteredChannelId(context)
      }
    }
  }
}