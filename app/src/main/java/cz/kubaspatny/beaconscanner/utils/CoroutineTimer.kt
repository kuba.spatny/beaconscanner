package cz.kubaspatny.beaconscanner.utils

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class CoroutineTimer {

    companion object {
        const val DEFAULT_REPEAT_MS: Long = 1000


        fun startTimer(delayMillis: Long = DEFAULT_REPEAT_MS, repeatMillis: Long = DEFAULT_REPEAT_MS, action: () -> Unit) = GlobalScope.launch {
            delay(delayMillis)

            while (true) {
                action()
                delay(repeatMillis)
            }
        }
    }

}