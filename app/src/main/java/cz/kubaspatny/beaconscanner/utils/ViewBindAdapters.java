package cz.kubaspatny.beaconscanner.utils;

import android.view.View;
import androidx.databinding.BindingAdapter;

/**
 * @author Jakub Janda
 */
public class ViewBindAdapters {
    @BindingAdapter({"visible"})
    public static void setVisible(View view,
                                  boolean visible) {
        view.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    @BindingAdapter({"invisible"})
    public static void setInvisible(View view,
                                    boolean invisible) {
        view.setVisibility(invisible ? View.INVISIBLE : View.VISIBLE);
    }


    @BindingAdapter({"enabled"})
    public static void setEnabled(View view,
                                  boolean enabled) {
        view.setEnabled(enabled);
    }


}
