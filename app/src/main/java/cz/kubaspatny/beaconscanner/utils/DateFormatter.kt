package cz.kubaspatny.beaconscanner.utils

import java.text.SimpleDateFormat
import java.util.*

class DateFormatter {

    companion object {
        private val formatter = SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.ENGLISH)

        fun formatDate(timestamp: Long): String {
            return formatter.format(Date(timestamp))
        }
    }

}