package cz.kubaspatny.beaconscanner.beacon

import java.lang.IllegalArgumentException

class EddystoneBeaconFactory {

    companion object {
        fun createBeaconInstance(address: String, serviceData: ByteArray): EddystoneBeacon {
            val typeByte = serviceData[0]

            return when (typeByte) {
                EddystoneUIDBeacon.frameType -> EddystoneUIDBeacon(address)
                EddystoneTLMBeacon.frameType -> EddystoneTLMBeacon(address)
                EddystoneUrlBeacon.frameType -> EddystoneUrlBeacon(address)
                else -> throw IllegalArgumentException("Unsupported frame type=$typeByte")
            }
        }
    }

}