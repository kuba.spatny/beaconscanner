package cz.kubaspatny.beaconscanner.beacon

import android.util.Log
import java.nio.ByteBuffer
import kotlin.experimental.and

class EddystoneTLMBeacon(
    address: String
) : EddystoneBeacon(address) {
    var version: String = ""
    var voltage: String = ""
    var temp: String = ""

    override fun updateFrameTypeSpecificData(serviceData: ByteArray) {
        if (serviceData.size < MIN_SERVICE_DATA_LEN) {
            val err = String.format(
                "TLM frame too short, needs at least %d bytes, got %d",
                MIN_SERVICE_DATA_LEN, serviceData.size
            )

            Log.d("EddystoneTLMBeacon", "Error: $err")
            return
        }

        val buf = ByteBuffer.wrap(serviceData)
        buf.get()  // We already know the frame type byte is 0x20.

        // The version should be zero.
        version = String.format("0x%02X", buf.get())

        // Battery voltage should be sane. Zero is fine if the device is externally powered, but
        // it shouldn't be negative or unreasonably high.
        voltage = buf.short.toString()

        // Temp varies a lot with the hardware and the margins appear to be very wide. USB beacons
        // in particular can report quite high temps. Let's at least check they're partially sane.
        val tempIntegral = buf.get()
        val tempFractional = (buf.get()) and (0xff.toByte())
        val temperature = tempIntegral + tempFractional / 256.0f
        temp = temperature.toString()
//
//        // Check the PDU count is increasing from frame to frame and is neither too low or too high.
//        val advCnt = buf.int
//        beacon.tlmStatus.advCnt = advCnt.toString()
//        if (advCnt <= 0) {
//            val err = "Expected TLM ADV count to be positive, got $advCnt"
//            beacon.tlmStatus.errPduCnt = err
//            logDeviceError(deviceAddress, err)
//        }
//        if (advCnt > MAX_EXPECTED_PDU_COUNT) {
//            val err = String.format(
//                "TLM ADV count %d is higher than expected max of %d",
//                advCnt, MAX_EXPECTED_PDU_COUNT
//            )
//            beacon.tlmStatus.errPduCnt = err
//            logDeviceError(deviceAddress, err)
//        }
//        if (previousTlm != null) {
//            val previousAdvCnt = ByteBuffer.wrap(previousTlm, 6, 4).int
//            if (previousAdvCnt == advCnt) {
//                val err = "Expected increasing TLM PDU count but unchanged from $advCnt"
//                beacon.tlmStatus.errPduCnt = err
//                logDeviceError(deviceAddress, err)
//            }
//        }
//
//        // Check that the time since boot is increasing and is neither too low nor too high.
//        val uptime = buf.int
//        beacon.tlmStatus.secCnt = String.format("%d (%d days)", uptime, TimeUnit.SECONDS.toDays((uptime / 10).toLong()))
//        if (uptime <= 0) {
//            val err = "Expected TLM time since boot to be positive, got $uptime"
//            beacon.tlmStatus.errSecCnt = err
//            logDeviceError(deviceAddress, err)
//        }
//        if (uptime > MAX_EXPECTED_SEC_COUNT) {
//            val err = String.format(
//                "TLM time since boot %d is higher than expected max of %d",
//                uptime, MAX_EXPECTED_SEC_COUNT
//            )
//            beacon.tlmStatus.errSecCnt = err
//            logDeviceError(deviceAddress, err)
//        }
//        if (previousTlm != null) {
//            val previousUptime = ByteBuffer.wrap(previousTlm, 10, 4).int
//            if (previousUptime == uptime) {
//                val err = "Expected increasing TLM time since boot but unchanged from $uptime"
//                beacon.tlmStatus.errSecCnt = err
//                logDeviceError(deviceAddress, err)
//            }
//        }
//
//        val rfu = Arrays.copyOfRange(serviceData, 14, 20)
//        for (b in rfu) {
//            if (b.toInt() != 0x00) {
//                val err = "Expected TLM RFU bytes to be 0x00, were " + Utils.toHexString(rfu)
//                beacon.tlmStatus.errRfu = err
//                logDeviceError(deviceAddress, err)
//                break
//            }
//        }
    }

    override fun toMap(): HashMap<String, Any> {
        val data = HashMap<String, Any>()
        data["type"] = "EddystoneTLMBeacon"
        data["address"] = address
        data["rssi"] = rssi
        data["version"] = version
        data["voltage"] = voltage
        data["temp"] = temp
        data["rawData"] = rawData

        return data
    }

    override fun toString(): String {
        return "EddystoneTLMBeacon[$address], rssi=$rssi, version=$version, voltage=$voltage, temp=$temp, rawData=$rawData"
    }

    companion object {
        // TODO: tests
        internal val MIN_SERVICE_DATA_LEN: Byte = 14

        // TLM frames only support version 0x00 for now.
        internal val EXPECTED_VERSION: Byte = 0x00

        // Minimum expected voltage value in beacon telemetry in millivolts.
        internal val MIN_EXPECTED_VOLTAGE = 500

        // Maximum expected voltage value in beacon telemetry in millivolts.
        internal val MAX_EXPECTED_VOLTAGE = 10000

        // Value indicating temperature not supported. temp[0] == 0x80, temp[1] == 0x00.
        internal val TEMPERATURE_NOT_SUPPORTED = -128.0f

        // Minimum expected temperature value in beacon telemetry in degrees Celsius.
        internal val MIN_EXPECTED_TEMP = 0.0f

        // Maximum expected temperature value in beacon telemetry in degrees Celsius.
        internal val MAX_EXPECTED_TEMP = 60.0f

        // Maximum expected PDU count in beacon telemetry.
        // The fastest we'd expect to see a beacon transmitting would be about 10 Hz.
        // Given that and a lifetime of ~3 years, any value above this is suspicious.
        internal val MAX_EXPECTED_PDU_COUNT = 10 * 60 * 60 * 24 * 365 * 3

        // Maximum expected time since boot in beacon telemetry.
        // Given that and a lifetime of ~3 years, any value above this is suspicious.
        internal val MAX_EXPECTED_SEC_COUNT = 10 * 60 * 60 * 24 * 365 * 3

        // The service data for a TLM frame should vary with each broadcast, but depending on the
        // firmware implementation a couple of consecutive TLM frames may be broadcast. Store the
        // frame only if few seconds have passed since we last saw one.
        internal val STORE_NEXT_FRAME_DELTA_MS = 3000

        val frameType: Byte = 0x20
    }

}