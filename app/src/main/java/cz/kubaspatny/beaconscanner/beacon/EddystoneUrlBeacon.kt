package cz.kubaspatny.beaconscanner.beacon

import cz.kubaspatny.beaconscanner.eddystone.UrlUtils

class EddystoneUrlBeacon(
    address: String
) : EddystoneBeacon(address) {
    var urlValue: String = ""
    var txPower: Int = 0

    override fun updateFrameTypeSpecificData(serviceData: ByteArray) {
        // Tx power should have reasonable values.
        txPower = serviceData[1].toInt()
        urlValue = UrlUtils.decodeUrl(serviceData)
    }

    override fun toMap(): HashMap<String, Any> {
        val data = HashMap<String, Any>()
        data["type"] = "EddystoneTLMBeacon"
        data["address"] = address
        data["rssi"] = rssi
        data["urlValue"] = urlValue
        data["txPower"] = txPower
        data["rawData"] = rawData

        return data
    }

    override fun toString(): String {
        return "EddystoneUrlBeacon[$address], rssi=$rssi, urlValue=$urlValue, txPower=$txPower, rawData=$rawData"
    }

    companion object {
        val frameType: Byte = 0x10
    }
}