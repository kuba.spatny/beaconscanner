package cz.kubaspatny.beaconscanner.beacon

import cz.kubaspatny.beaconscanner.eddystone.Utils
import java.util.*
import kotlin.collections.HashMap

class EddystoneUIDBeacon(
    address: String
) : EddystoneBeacon(address) {
    var uidHexValue: String = ""
    var txPower: Int = 0


    override fun updateFrameTypeSpecificData(serviceData: ByteArray) {
        // Tx power should have reasonable values.
        txPower = serviceData[1].toInt()

        // The namespace and instance bytes should not be all zeroes.
        val uidBytes = Arrays.copyOfRange(serviceData, 2, 18)
        uidHexValue = Utils.toHexString(uidBytes)
    }

    private val namespaceId: String
        get() = uidHexValue.substring(0, 20)

    private val instanceId: String
        get() = uidHexValue.substring(20, 32)

    override fun toMap(): HashMap<String, Any> {
        val data = HashMap<String, Any>()
        data["type"] = "EddystoneUIDBeacon"
        data["address"] = address
        data["rssi"] = rssi
        data["namespaceId"] = namespaceId
        data["instanceId"] = instanceId
        data["txPower"] = txPower
        data["rawData"] = rawData

        return data
    }

    override fun toString(): String {
        return "EddystoneUIDBeacon[$address], rssi=$rssi, namespaceId=$namespaceId, instanceId=$instanceId, txPower=$txPower, rawData=$rawData"
    }

    companion object {
        val frameType: Byte = 0x00
    }
}