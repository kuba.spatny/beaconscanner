package cz.kubaspatny.beaconscanner.beacon

import android.os.ParcelUuid
import cz.kubaspatny.beaconscanner.eddystone.Utils

abstract class EddystoneBeacon(val address: String) {
    var rssi: Int = 0
    var firstSeenTs: Long = 0
    var lastSeenTs: Long = 0
    var rawData: String = ""

    abstract fun toMap() : HashMap<String, Any>

    internal abstract fun updateFrameTypeSpecificData(serviceData: ByteArray)

    fun updateFrameData(rssi: Int, serviceData: ByteArray) {
        val timestamp = System.currentTimeMillis()

        if(firstSeenTs == 0L) {
            firstSeenTs = timestamp
        }

        lastSeenTs = timestamp

        this.rssi = rssi
        rawData = Utils.toHexString(serviceData)
        updateFrameTypeSpecificData(serviceData)
    }

    companion object {
        /**
         * Minimum expected Tx power (in dBm) in UID and URL frames.
         */
        const val MIN_EXPECTED_TX_POWER = -100

        /**
         * Maximum expected Tx power (in dBm) in UID and URL frames.
         */
        const val MAX_EXPECTED_TX_POWER = 20

        // The Eddystone Service UUID, 0xFEAA.
        val EDDYSTONE_SERVICE_UUID: ParcelUuid = ParcelUuid.fromString("0000FEAA-0000-1000-8000-00805F9B34FB")
    }
}