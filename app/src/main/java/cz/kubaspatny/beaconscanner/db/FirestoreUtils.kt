package cz.kubaspatny.beaconscanner.db

import com.google.firebase.firestore.FirebaseFirestore
import cz.kubaspatny.beaconscanner.beacon.EddystoneBeacon
import cz.kubaspatny.beaconscanner.utils.DateFormatter

class FirestoreUtils(private val streamName: String, private val db: FirebaseFirestore) {

    fun initStream(timestamp: Long) {
        val data = HashMap<String, Any>()
        data["ts"] = timestamp
        data["formattedTime"] = DateFormatter.formatDate(timestamp)

        db.collection("streams").document(streamName).set(data)
    }

    fun addBeaconBatch(timestamp: Long, beacons: List<EddystoneBeacon>) {
        val data = HashMap<String, Any>()
        data["ts"] = timestamp
        data["formattedTime"] = DateFormatter.formatDate(timestamp)
        data["beacons"] = beacons.map { beacon -> beacon.toMap() }

        db.collection("streams").document(streamName).collection("data").add(data)
    }

}