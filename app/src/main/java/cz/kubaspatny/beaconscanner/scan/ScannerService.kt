package cz.kubaspatny.beaconscanner.scan

import android.app.Service
import android.bluetooth.BluetoothManager
import android.bluetooth.le.*
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.lifecycle.LifecycleService
import com.google.firebase.firestore.FirebaseFirestore
import cz.kubaspatny.beaconscanner.beacon.EddystoneBeacon
import cz.kubaspatny.beaconscanner.beacon.EddystoneBeacon.Companion.EDDYSTONE_SERVICE_UUID
import cz.kubaspatny.beaconscanner.beacon.EddystoneBeaconFactory
import cz.kubaspatny.beaconscanner.db.FirestoreUtils
import cz.kubaspatny.beaconscanner.model.ScannerServiceConnector
import cz.kubaspatny.beaconscanner.notification.ServiceNotification
import cz.kubaspatny.beaconscanner.utils.CoroutineTimer
import cz.kubaspatny.beaconscanner.utils.DateFormatter
import cz.seznam.kommons.kexts.startServiceInForeground
import kotlinx.coroutines.Job
import java.util.*
import com.google.firebase.firestore.FirebaseFirestoreSettings



class ScannerService : LifecycleService() {

    val notification: ServiceNotification by lazy {
        ServiceNotification(baseContext)
    }

    private var timer: Job? = null
    private var db: FirestoreUtils? = null

    // An aggressive scan for nearby devices that reports immediately.
    private val scanSettings = ScanSettings.Builder()
        .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
        .setReportDelay(0)
        .build()

    private val scanFilters: List<ScanFilter> =
        ArrayList<ScanFilter>().apply { add(ScanFilter.Builder().setServiceUuid(EDDYSTONE_SERVICE_UUID).build()) }

    private val scanner: BluetoothLeScanner? by lazy(LazyThreadSafetyMode.NONE) {
        val bluetoothManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        bluetoothManager.adapter.bluetoothLeScanner
    }

    private val beacons = HashMap<String, EddystoneBeacon>()

    private val scanCallback = object : ScanCallback() {
        override fun onScanResult(callbackType: Int, result: ScanResult?) {
            val result = result ?: return

            val scanRecord = result.scanRecord ?: return
            val deviceAddress = result.device.address
            val serviceData = scanRecord.getServiceData(EDDYSTONE_SERVICE_UUID) ?: return

            val previousBeaconsCount = beacons.size
            val beacon: EddystoneBeacon = beacons.getOrPut(deviceAddress) {
                EddystoneBeaconFactory.createBeaconInstance(
                    deviceAddress,
                    serviceData
                )
            }
            beacon.updateFrameData(result.rssi, serviceData)

            if(previousBeaconsCount != beacons.size) {
                ScannerServiceConnector.beaconsCount.value = beacons.size
            }

            Log.d(TAG, "Scanned: $beacon")
        }

        override fun onScanFailed(errorCode: Int) {
            when (errorCode) {
                ScanCallback.SCAN_FAILED_ALREADY_STARTED -> Log.d(TAG, "SCAN_FAILED_ALREADY_STARTED")
                ScanCallback.SCAN_FAILED_APPLICATION_REGISTRATION_FAILED -> Log.d(TAG,"SCAN_FAILED_APPLICATION_REGISTRATION_FAILED")
                ScanCallback.SCAN_FAILED_FEATURE_UNSUPPORTED -> Log.d(TAG, "SCAN_FAILED_FEATURE_UNSUPPORTED")
                ScanCallback.SCAN_FAILED_INTERNAL_ERROR -> Log.d(TAG, "SCAN_FAILED_INTERNAL_ERROR")
                else -> Log.d(TAG, "Scan failed, unknown error code")
            }
        }
    }

    override fun onStartCommand(
        intent: Intent?,
        flags: Int,
        startId: Int
    ): Int {
        super.onStartCommand(intent, flags, startId)

        when (intent?.action) {
            ACTION_START_SCANNING -> startScanning()
            ACTION_STOP_SCANNING -> stopScanning()
        }

        return Service.START_STICKY
    }

    override fun onDestroy() {
        ScannerServiceConnector.serviceScanning.value = false
        super.onDestroy()
    }

    private fun startScanning() {
        Log.d(TAG, "startScanning()")
        startForeground(notification.id, notification.notification)
        ScannerServiceConnector.serviceScanning.value = true

        val firestoreInstance = FirebaseFirestore.getInstance().apply {
            val settings = FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(true)
                .build()

            firestoreSettings = settings
        }

        db = FirestoreUtils(UUID.randomUUID().toString(), firestoreInstance).apply {
            initStream(System.currentTimeMillis())
        }

        scanner?.startScan(scanFilters, scanSettings, scanCallback)
        timer = CoroutineTimer.startTimer { printRelevantBeacons() }
    }

    private fun stopScanning() {
        Log.d(TAG, "stopScanning()")

        timer?.cancel()
        scanner?.stopScan(scanCallback)
        beacons.clear()

        ScannerServiceConnector.serviceScanning.value = false
        ScannerServiceConnector.beaconsCount.value = 0
        stopForeground(true)
        stopSelf()
    }

    private fun printRelevantBeacons() {
        val currentTimeMs = System.currentTimeMillis()
        val maxAge = 5000

        Log.d(TAG, "-------------------------------------------------------")
        Log.d(TAG, "Current time: ${DateFormatter.formatDate(System.currentTimeMillis())}")

        beacons.values.filter { beacon -> (currentTimeMs - beacon.lastSeenTs) < maxAge }.apply {
            if(isEmpty()) return@apply

            db?.addBeaconBatch(currentTimeMs, this)

            forEach { beacon ->
                if((currentTimeMs - beacon.lastSeenTs) < maxAge) {
                    Log.d(TAG, beacon.toString())
                }
            }
        }

        Log.d(TAG, "-------------------------------------------------------")
    }


    companion object {
        private const val ACTION_START_SCANNING = "startScanning"
        private const val ACTION_STOP_SCANNING = "stopScanning"
        private const val TAG = "ScannerService"


        fun startScanning(context: Context) {
            val intent = Intent(ACTION_START_SCANNING)
            intent.setClass(context, ScannerService::class.java)
            context.startServiceInForeground(intent)
        }

        fun stopScanning(context: Context) {
            val intent = Intent(ACTION_STOP_SCANNING)
            intent.setClass(context, ScannerService::class.java)
            context.startService(intent)
        }
    }

}