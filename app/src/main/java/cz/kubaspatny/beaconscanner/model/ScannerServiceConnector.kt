package cz.kubaspatny.beaconscanner.model

import androidx.lifecycle.MutableLiveData

class ScannerServiceConnector {

    companion object {
        val serviceScanning: MutableLiveData<Boolean> = MutableLiveData<Boolean>().apply { value = false }
        val beaconsCount: MutableLiveData<Int> = MutableLiveData<Int>().apply { value = 0 }
    }

}