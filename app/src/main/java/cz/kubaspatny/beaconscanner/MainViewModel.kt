package cz.kubaspatny.beaconscanner

import androidx.lifecycle.MutableLiveData
import cz.kubaspatny.beaconscanner.model.ScannerServiceConnector

class MainViewModel {

    val bluetoothEnabled = MutableLiveData<Boolean>().apply { value = false }
    val locationEnabled = MutableLiveData<Boolean>().apply { value = false }
    val serviceScanning = ScannerServiceConnector.serviceScanning
    val beaconsCount = ScannerServiceConnector.beaconsCount

}