package cz.kubaspatny.beaconscanner

import android.Manifest
import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import cz.kubaspatny.beaconscanner.scan.ScannerService


class MainActivity : AppCompatActivity() {

    private val viewModel = MainViewModel()

    private val bluetoothAdapter: BluetoothAdapter? by lazy(LazyThreadSafetyMode.NONE) {
        val bluetoothManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        bluetoothManager.adapter
    }

    private val BluetoothAdapter.isDisabled: Boolean
        get() = !isEnabled

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val uiBind = DataBindingUtil.setContentView<cz.kubaspatny.beaconscanner.databinding.ActivityMainBinding>(
            this,
            R.layout.activity_main
        )
        uiBind.lifecycleOwner = this
        uiBind.viewModel = viewModel
        uiBind.startScan.setOnClickListener {
            startScanning()
        }

        uiBind.stopScan.setOnClickListener {
            stopScanning()
        }
    }

    override fun onResume() {
        super.onResume()

        checkBluetoothAdapter()

        viewModel.locationEnabled.value = hasFineLocationPermission(this)

        if (viewModel.locationEnabled.value == false) {
            requestFineLocationPermission()
        }
    }

    private fun checkBluetoothAdapter() {
        viewModel.bluetoothEnabled.value = bluetoothAdapter?.isEnabled ?: false

        // Ensures Bluetooth is available on the device and it is enabled. If not,
        // displays a dialog requesting user permission to enable Bluetooth.
        bluetoothAdapter?.takeIf { it.isDisabled }?.apply {
            val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT)
        }
    }

    private fun startScanning() {
        if (viewModel.serviceScanning.value == true) {
            Log.d("MainActivity", "startScanning: scan already started")
            return
        }

        ScannerService.startScanning(baseContext)
    }

    private fun stopScanning() {
        if (viewModel.serviceScanning.value == false) {
            Log.d("MainActivity", "stopScanning: scan already stopped")
            return
        }

        ScannerService.stopScanning(baseContext)
    }


    private fun hasFineLocationPermission(context: Context): Boolean {
        return try {
            ContextCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        } catch (e: RuntimeException) {
            false
        }
    }

    private fun requestFineLocationPermission() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
            REQUEST_PERMISSION_FINE_LOCATION
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            REQUEST_ENABLE_BT -> {
                viewModel.bluetoothEnabled.value = resultCode == Activity.RESULT_OK
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode) {
            REQUEST_PERMISSION_FINE_LOCATION -> {
                viewModel.locationEnabled.value =
                    grantResults.size == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED
            }
        }
    }

    companion object {
        private const val REQUEST_ENABLE_BT: Int = 1
        private const val REQUEST_PERMISSION_FINE_LOCATION: Int = 2
        private const val SCAN_PERIOD_MS: Long = 20000 // 10s
    }
}
